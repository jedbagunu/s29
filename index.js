db.hr.insertMany([
	{
	firstName: "Jane",
	lastName: "Doe",
	age: 25,
	contact: {
		phone: "09234544356",
		email: "jd@zuit.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
},
	{
	firstName: "sarah",
	lastName: "Reyes",
	age: 75,
	contact: {
		phone: "09234544356",
		email: "ry@zuit.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
},
	{
	firstName: "Donald",
	lastName: "Carter",
	age: 30,
	contact: {
		phone: "09234544356",
		email: "cd@zuit.com"
	},
	courses: ["Laravel", "JSON", "React"],
	department: "none"
}
])

 /*2. Find users with letter s in their first name or d in their last name.
a. Use the $or operator.
b. Show only the firstName and lastName fields and hide the _id field*/

db.hr.find ({
	$or: [
		{
			firstName: {$regex: 's', $options: 'i'}
		},
		{
			firstName: {$regex:'d', $options:'i'}
		},
                {
		firstName: 1,
		lastName: 1,
		_id:0
	}
	]

                
})
/*3. Find users who are from the HR department and their age is greater
than or equal to 70.
a. Use the $and operator
*/
db.hr.find ({
	$and: [
		{
			age: {$gt: 70}
		},{
			age: {$gte:50}
		}
	]
})

/*4. Find users with the letter e in their first name and has an age of less
than or equal to 30.
a. Use the $and, $regex and $lte operators
*/

db.hr.find ({
	$and: [
		{
			firstName: {$regex: 'e', $options: 'i'}
		},
		{
			age: {$lte: 30}
		}

	]

})